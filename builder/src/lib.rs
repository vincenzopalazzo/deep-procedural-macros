use kproc_parser::kproc_macros::KTokenStream;
use kproc_parser::proc_macros::TokenStream as TokenStreamV2;
use kproc_parser::rust::ast::RustAST;
use kproc_parser::rust::rust_struct::parse_struct;
use proc_macro::TokenStream;

#[proc_macro_derive(Builder)]
pub fn derive(input: TokenStream) -> TokenStream {
    let input_v2 = TokenStreamV2::from(input);
    let mut kstream = KTokenStream::new(&input_v2);
    let ast = parse_struct(&mut kstream);
    // FIXME: avoid to unwrap, this is the perfect place where
    // span a suggestion
    generate_impl(&ast).unwrap()
}

fn generate_impl(ast_struct: &RustAST) -> Result<TokenStream, String> {
    match ast_struct {
        RustAST::Struct(_, name, attrs) => {
            let mut code = format!("impl {name} {{\n");
            for attr in attrs {
                let res = generate_build_function(attr);
                match res {
                    Ok(res) => {
                        code += res.as_str();
                    }
                    // FIXME: good place to span a compiler error
                    Err(err) => panic!("{err}"),
                }
            }
            code += "}";
            let code = code.parse().unwrap();
            Ok(code)
        }
        _ => Err("AST root it is not a struct".to_owned()),
    }
}

fn generate_build_function(attr: &RustAST) -> Result<String, String> {
    match attr {
        RustAST::Field(_, name, ty) => {
            let mut code = format!("pub fn {name}(&mut self, inner: {ty}) -> Self {{\n");
            code += format!("   self.{name} = inner;\n").as_str();
            code += "}\n";
            Ok(code)
        }
        _ => Err("AST root it is not a filed".to_owned()),
    }
}
