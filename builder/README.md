# Builder Procedural Macros Project

From the [proc-macro-workshop](https://github.com/dtolnay/proc-macro-workshop) this project is the
first project to start to work with the procedural macros, and a good description of the project
is five by the original author reported below.

### Derive macro: `derive(Builder)`

This macro generates the boilerplate code involved in implementing the [builder
pattern] in Rust. Builders are a mechanism for instantiating structs, especially
structs with many fields, and especially if many of those fields are optional or
the set of fields may need to grow backward compatibly over time.

[builder pattern]: https://en.wikipedia.org/wiki/Builder_pattern

There are a few different possibilities for expressing builders in Rust. Unless
you have a strong pre-existing preference, to keep things simple for this
project I would recommend following the example of the standard library's
[`std::process::Command`] builder in which the setter methods each receive and
return `&mut self` to allow chained method calls.

[`std::process::Command`]: https://doc.rust-lang.org/std/process/struct.Command.html

Callers will invoke the macro as follows.

```rust
use derive_builder::Builder;

#[derive(Builder)]
pub struct Command {
    executable: String,
    #[builder(each = "arg")]
    args: Vec<String>,
    current_dir: Option<String>,
}

fn main() {
    let command = Command::builder()
        .executable("cargo".to_owned())
        .arg("build".to_owned())
        .arg("--release".to_owned())
        .build()
        .unwrap();

    assert_eq!(command.executable, "cargo");
}
```

This project covers:

- traversing syntax trees;
- constructing output source code;
- processing helper attributes to customize the generated code.

*Project skeleton is located under the <kbd>builder</kbd> directory.*
