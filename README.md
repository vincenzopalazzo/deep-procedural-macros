# deep-procedural-macros

A deep dive on procedural macros course based on proc-macro-workshop from @dtolnay 

To see the basic introduction to it please take in consideration to see the original repository 
https://github.com/dtolnay/proc-macro-workshop

This repository point to give a blog post series to make the procedural macros more approachable
for the people that are start with it.


To read more about the [Procedural Macros are your super power with Rust]() consider to jump in
my personal blog.

## Licence

<sub>
Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in this codebase by you, as defined in the Apache-2.0 license,
shall be dual licensed as above, without any additional terms or conditions.
</sub>
